import React from 'react';
import './App.css';
import Chat from './chat/chat/Chat';

function App() {
  return (
    <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
  );
}

export default App;
