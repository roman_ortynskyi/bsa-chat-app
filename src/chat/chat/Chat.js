import React, { 
    useState, 
    useEffect, 
    createContext, 
    useRef
} from 'react';
import ChatHeader from '../header/Header';
import MessageList from '../message-list/MessageList';
import MessageInput from '../message-input/MessageInput';

import './Chat.css';

export const ChatContext = createContext();

function Chat({ url }) {
    const [messages, setMessages] = useState([]);    
    const messageListRef = useRef();

    useEffect(() => {
        (async () => {
            const response = await fetch(url, {
                method: 'GET'
            });
            const data = await response.json();

            const messages = data.map(message => ({ ...message, isLiked: false }));

            setMessages(messages);
        })();
    }, [url]);

    const contextValue = {
        messages,
        setMessages,
        messageListRef
    };

    const usersCount = new Set(messages.map(message => message.user)).size;
    const lastMessageCreatedAt = messages[messages.length - 1]?.createdAt;

    const deleteMessage = (id) => {
        const filteredMessages = messages.filter(message => message.id !== id);
        setMessages(filteredMessages);
    };

    const editMessage = (id) => {
        const messageIndex = messages.findIndex(el => el.id === id);

        const message = messages[messageIndex];

        const text = prompt();

        const updatedMessage = {
            ...message,
            text
        }

        const updatedMessages = [
            ...messages.slice(0, messageIndex),
            updatedMessage,
            ...messages.slice(messageIndex + 1)
        ];

        setMessages(updatedMessages);
    };

    const likeMessage = (id) => {
        const messageIndex = messages.findIndex(el => el.id === id);

        const message = messages[messageIndex];

        const updatedMessage = {
            ...message,
            isLiked: !message.isLiked
        }

        const updatedMessages = [
            ...messages.slice(0, messageIndex),
            updatedMessage,
            ...messages.slice(messageIndex + 1)
        ];

        setMessages(updatedMessages);
    }

    return (
        <ChatContext.Provider value={contextValue}> 
            <div className="chat">
                <ChatHeader 
                    messagesCount={messages.length}
                    usersCount={usersCount}
                    lastMessageCreatedAt={lastMessageCreatedAt}
                />
                <MessageList
                    messages={messages} 
                    onDeleteMessage={deleteMessage} 
                    onEditMessage={editMessage} 
                    onLikeMessage={likeMessage}
                />
                <MessageInput />
            </div>
        </ChatContext.Provider>
    );
}

export default Chat;