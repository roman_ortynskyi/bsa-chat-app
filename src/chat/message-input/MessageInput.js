import React, { 
    useContext, 
    useState,
    useEffect 
} from 'react';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import { ChatContext } from '../chat/Chat';

import './MessageInput.css';

function ChatForm() {
    const [message, setMessage] = useState('');
    const { messages, setMessages, messageListRef } = useContext(ChatContext);

    const handleMessageChange = (e) => {
        setMessage(e.target.value);
    };

    const handleSend = () => {
        const newMessage = {
            id: uuidv4(),
            text: message, 
            isMine: true,
            createdAt: new Date().toISOString()
        };

        setMessages(prevMessages => [...prevMessages, newMessage]);
        setMessage('');
    };

    useEffect(() => {
        messageListRef.current.scrollTop = messageListRef.current.scrollHeight;
    }, [messages, messageListRef]);

    return (
        // <div id="chat-form">
        //     <input 
        //         type="text" 
        //         placeholder="type a message" 
        //         onChange={handleMessageChange}
        //     />
        //     <button onClick={handleSend}>Send</button>
        // </div>
        <div className="message-input">
            <input
                className="message-input-text" 
                type="text" 
                placeholder="type a message" 
                onChange={handleMessageChange}
                value={message}
            />
            <button
                className="message-input-button"
                disabled={message.length === 0}
                onClick={handleSend}
            >Send</button>
        </div>
    );
}

export default ChatForm;