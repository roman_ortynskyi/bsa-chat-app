import React from 'react';
import moment from 'moment';

import './Message.css';

function Message(props) {
    const {
        id,
        avatar,
        text,
        createdAt,
        user,
        isLiked,
        onLike
    } = props;

    const time = moment(createdAt).format('hh:mm');

    const likeButtonText = isLiked ? 'Dislike' : 'Like';

    const likeButtonClassName = isLiked ? 'message-liked' : 'message-like';

    return (
        // <div className="message">
        //         <div className="message-content">
        //             <img src={require("../../images/profiles/daryl.png")} alt={user} />
        //             {/* <img className="message-user-avatar" src={avatar} alt={user} /> */}
        //             <span className="message-user-name">{user}</span>
        //             <div className="message-text">{text}</div>
        //             <div className="message-time">{createdAt}</div>
        //         </div>
        //     </div>
        <div className="message other-message">
            <div className="message-content">
                <img className="message-user-avatar" src={avatar} alt={user} />
                <div className="message-text">{text}</div>
                <div className="message-time">{time}</div>
                <button className={likeButtonClassName} onClick={() => onLike(id)}>{likeButtonText}</button>
            </div>
        </div>
    );
}

export default Message;